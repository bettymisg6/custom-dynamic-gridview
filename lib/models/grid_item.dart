import 'package:flutter/material.dart';

import '../widgets/widgets.dart';
class GridItem {
  final Color color;
  final double spacing;
  final int flex;
  final Widget child;

  GridItem(
      {required this.color,
      required this.spacing,
      required this.flex,
      required this.child});
}
 List<GridItem> gridItems = [
    GridItem(
      color: Colors.pinkAccent,
      spacing: 10.0,
      flex: 1,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          CustomImageContainer(
            image: NetworkImage(
                "https://th.bing.com/th/id/OIP.qDatXiEQIWquWNoyXMvwGwHaIt?pid=ImgDet&w=200&h=235&c=7&dpr=1.5"),
            width: 230,
            height: 160,
          ),
        ],
      ),
    ),
    GridItem(
      color: Colors.deepPurple,
      spacing: 20.0,
      flex: 2,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          Icon(
            Icons.person,
            size: 50.0,
            color: Colors.white,
          ),
          CustomTextWidget(
            text: 'Profile',
            style: TextStyle(
              fontSize: 20.0,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    ),
    GridItem(
      color: Colors.black,
      spacing: 30.0,
      flex: 3,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          Icon(
            Icons.settings,
            size: 50.0,
            color: Colors.orange,
          ),
          CustomTextWidget(
            text: 'Settings',
            style: TextStyle(
              fontSize: 20.0,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    ),
    GridItem(
      color: Colors.yellow,
      spacing: 0.0,
      flex: 1,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          CustomListTile(
            leading: Icon(
              Icons.person,
              color: Colors.black,
              size: 25,
            ),
            title: Text(
              'Betty M.',
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
            ),
            subtitle: Text('Software Eng.'),
          ),
          CustomListTile(
            leading: Icon(Icons.location_on,color: Colors.black,),
            title: Text('Addis Ababa',  style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.bold),),
          ),
        ],
      ),
    ),
    GridItem(
      color: Colors.purple,
      spacing: 10.0,
      flex: 1,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CustomListTile(
            leading: const CircleAvatar(backgroundColor: Colors.white,radius: 30,child: Text("B",  style:
                  TextStyle(color: Colors.purple, fontWeight: FontWeight.bold),)),
            title: const Text('Betty M.', style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
            onTap: () {
            },
          ),
        ],
      ),
    ),
    GridItem(
      color: Colors.white,
      spacing: 20.0,
      flex: 1,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          CustomImageContainer(
            image: NetworkImage(
                "https://th.bing.com/th/id/OIP.r3osY2wZ4VEZUVU87UyhuwHaHa?pid=ImgDet&rs=1"),
            width: 150,
            height: 100,
          ),
        ],
      ),
    ),
  ];