import 'package:assi1/models/grid_item.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const CustomGridWidget());
}

class CustomGridWidget extends StatelessWidget {
  const CustomGridWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: const Text('Grid of Custom Widgets'),
          centerTitle: true,
          elevation: 0,
        ),
        body: GridView.builder(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
          ),
          itemCount: gridItems.length,
          itemBuilder: (BuildContext context, int index) {
            final item = gridItems[index];
            return _buildGridItem(item);
          },
        ),
      ),
    );
  }

  Widget _buildGridItem(GridItem item) {
    return Container(
      color: item.color,
      margin: EdgeInsets.all(item.spacing),
      child: Flex(
        direction: Axis.vertical,
        children: [
          Expanded(
            flex: item.flex,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: item.child,
            ),
          ),
        ],
      ),
    );
  }
}
