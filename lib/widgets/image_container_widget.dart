import 'package:flutter/material.dart';

class CustomImageContainer extends StatelessWidget {
  final ImageProvider image;
  final double width;
  final double height;

  const CustomImageContainer({super.key, 
    required this.image,
    required this.width,
    required this.height,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: image,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}